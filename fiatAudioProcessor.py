# -*- coding: utf-8 -*-

# Fiat Blue&Me Audio Processor
# Written by: Fabio Arlati (arlati.fabio@gmail.com)

# Requirements: python 2.6/7, python-mutagen
#
# USAGE: copy the script in the root of your music collection and launch "python fiatAudioProcessor.py"
#
# INPUT: MP3 Folder following this template:
# 	root
#	 |__[Artist1]
#	 |	 |__[Year] - [Album]
#	 |	 |		 |__01 - [Track01]
#	 |	 |		 |__02 - [Track02]
#	 |	 |		 |__...
#	 |	 |		 |__NN - [TrackNN]
#	 |	 |__[Year] - [Album]
#	 |			 |__XX - [TrackXX]
#	 |			 |__...
#	 |__[Artist2]
#	 |	 |__[Year] - [Album]
#	 |			 |__XX - [TrackXX]
#	 |			 |__...
#	 |__[Year] - [MixName]
#		 	 |__[Artist01] - [Track01]
#		 	 |__[Artist02] - [Track02]
#			 |__...
#		 	 |__[ArtistNN] - [TrackNN]
#
# OUTPUT:	- MP3 cleaned from critical characters (see list)
#		- Sorted WPL playlist for any folder, in particular:
#			- 01-Full collection
#			- A playlist for each artist
#			- A playlist for each album
#			- A playlist for each mix
#		- [Optional] Uncomment lines below to remove any ID3/APE tag from your MP3 file
#
# NOTES & DISCLAIMER:	It is strongly suggested to make a backup of your MP3 collection in order to keep
#			copies of the original filenames and tags.
#			I will not be responsible for any damage or loss of any MP3 in the user's collection

import os
import sys
import codecs
import mutagen.apev2
from mutagen.mp3 import MP3
from lxml import etree

TEXT_ENCODING = 'utf8'
GLOBAL_COUNTER = 0

# !! Add any critical character in your music collection to this list.
critical_chars = ['"', "'", "&", "$", "ñ"]
def containsCritical(filename):
	for crit in critical_chars:
		if (filename.find(crit)!="-1"):
			return True
	return False
def cleanCritical(filename):
	for crit in critical_chars:
		filename = filename.replace(crit," ")
	return filename

baseDir = os.getcwd()

def generateXml(albumName,filelist):
	print 'Creating album '+albumName
	# create XML 
	smil = etree.Element('smil')
	head = etree.Element('head')
	smil.append(head)
	head.append(etree.Element('meta', content="Microsoft Windows Media Player - PostProcessed by arlix85", name="Generator"))
	head.append(etree.Element('meta', content=str(len(filelist)), name="ItemCount"))
	head.append(etree.Element('meta', name="IsFavorite"))
	head.append(etree.Element('meta', name="ContentPartnerListID"))
	head.append(etree.Element('meta', name="ContentPartnerNameType"))
	head.append(etree.Element('meta', name="ContentPartnerName"))
	head.append(etree.Element('meta', name="Subtitle"))
	head.append(etree.Element('author'))
	title = etree.Element('title')
	title.text = albumName
	head.append(title)
	
	body = etree.Element('body')
	smil.append(body)
	seq = etree.Element('seq')
	body.append(seq)
	for fn in filelist:
		try:
			seq.append(etree.Element('media', src=fn.replace("/","\\")))
		except:
			print '!!!!! Error on file '+fn+'!!!!!'
	
	s = etree.tostring(smil, pretty_print=True)
	out_file = open(albumName+".wpl","w")
	out_file.write('<?wpl version="1.0"?>\n'+s)
	out_file.close()
	print 'Completed album '+albumName+'!'

def processDirectory(fpath,dirname='All',upperdirname=''):
	global GLOBAL_COUNTER
	GLOBAL_COUNTER += 1
	albumName = str(GLOBAL_COUNTER).zfill(2)
	if (len(dirname.split('-'))>1):
		splitName = ''
		for i in range(1,len(dirname.split('-'))):
			if (splitName):
				splitName += '-'
			splitName += dirname.split('-')[i].strip()
		splitName += '-'+dirname.split('-')[0].strip()
		albumName += '-'+splitName
	else:
		if(dirname):
			albumName += '-'+dirname
	if (upperdirname):
		albumName += '-' + upperdirname
	filelist = []
	for fn in sorted(os.listdir(fpath)):
		fname = os.path.join(fpath, fn)
		if(os.path.isdir(fname)):
			filelist.extend(processDirectory(fname,fn,dirname))
		else:
			if fname.lower().endswith('.mp3'):
				if (containsCritical(fn)):
					newFn = cleanCritical(fn)
					os.rename(fname,os.path.join(fpath, newFn))
					fname = os.path.join(fpath, newFn)
				filelist.append(fname.split(baseDir+'/',1)[1])
# !! Uncomment here to clean MP3 from ID3/APE tags
#				mp3 = MP3(fname)
#				try:
#					mutagen.apev2.delete(fname)
#					mp3.delete()
#					mp3.save()
#				except:
#					pass
	print 'Processed '+fpath+'...'
	if(len(filelist)>0):
		generateXml(albumName,filelist)
	return filelist

processDirectory(baseDir)


